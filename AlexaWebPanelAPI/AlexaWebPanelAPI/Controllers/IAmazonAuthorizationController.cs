﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace AlexaWebPanelAPI.Controllers
{
    public interface IAmazonAuthorizationController
    {
        Task<IActionResult> HttpGetAccessToken(string code);
    }
}