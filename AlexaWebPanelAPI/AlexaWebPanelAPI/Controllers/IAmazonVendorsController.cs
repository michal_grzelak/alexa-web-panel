﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AlexaWebPanelAPI.Controllers
{
    public interface IAmazonVendorsController
    {
        Task<IActionResult> HttpGetVendorList();
    }
}