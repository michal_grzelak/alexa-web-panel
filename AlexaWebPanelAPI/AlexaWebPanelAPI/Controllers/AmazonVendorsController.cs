﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AlexaWebPanelAPI.Lib.ExtensionMethods;
using AlexaWebPanelAPI.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace AlexaWebPanelAPI.Controllers
{
    [Route("amazon/api")]
    [ApiController]
    public class AmazonVendorsController : BaseApi, IAmazonVendorsController
    {
        public AmazonVendorsController(IHttpClientFactory httpClientFactory, IConfiguration configuration) : base(httpClientFactory, configuration) { }

        [HttpGet("v1/vendors")]
        public async Task<IActionResult> HttpGetVendorList()
        {
            string token = "Atza|IwEBIBWSsXZguD3JHa5CkwzptQc24miPpIp-kPknGED5DZqiuYXTshlf2GkS41luxw4zK2c_3Eg6W6LwvaUsKjoqgzvqT5Wrs2SK-CFj0cSL5UQ32hzqhA_Q_dOpf1L3PYljozZtCf_7Fyrkqx3kaMQ5EeclVxP3015E10mjpcyPZOCeM1bpdlw-BgxAYYdmXyVrJSYA-bu27abkDFf5hk-F-tlLoFwQL8RC0q9rzedLi_-C9IAUY-PNV1gLySo7MEma4q5NQWsB9oJbs7keUz16bMLyLGud_Tej71ll9wrGS8OrxkEQMa340RAKgYsywsxf8Zgfdrm-ZhBl8DyccygPzi3e-lxZlFL9m5Pmam-voNErmMX6Wv48rv8eMsBJ9NkASix6-VbFko20kWsNCJFzUHakQlPJJYAKKmAKOParzkOzypaUV4YhN0v_9CBCh-vhELrYSzf5BJxVai4Ss0BwkgXsG7stKysg74dsbfEvlSTgin00h41OKiUFkaN_saEkt7af-1qvnI8SUnoNkkWoufXan8EKRerFsdATvjqS5_H3EAXNp-_d3ZAtaw3tCoeQp3w";
            HttpBody body = new HttpBody();
            body.Add("Authorization", token);

            HttpClient client = _httpClientFactory.CreateClient();

            return await client.SendRequest(string.Format("{0}/v1/vendors", _amazonApi), HttpMethod.Get, body.Json);
        }
    }
}