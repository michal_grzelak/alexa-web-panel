﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AlexaWebPanelAPI.Lib.ExtensionMethods;
using AlexaWebPanelAPI.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace AlexaWebPanelAPI.Controllers
{
    [Route("amazon/auth")]
    [ApiController]
    public class AmazonAuthorizationController : BaseApi, IAmazonAuthorizationController
    {
        public AmazonAuthorizationController(IHttpClientFactory httpClientFactory, IConfiguration configuration) : base(httpClientFactory, configuration) { }

        [HttpPost("token/refresh")]
        public async Task<IActionResult> HttpRefreshAccessToken([FromBody]string token)
        {
            HttpBody body = new HttpBody();
            body.Add("refresh_token", token);
            body.Add("client_secret", _clientSecret);
            body.Add("client_id", _clientId);
            body.Add("grant_type", "refresh_token");

            HttpClient client = _httpClientFactory.CreateClient();

            return await client.SendRequest(_amazonAuth, HttpMethod.Post, body);
        }

        [HttpGet("token/{code}")]
        public async Task<IActionResult> HttpGetAccessToken(string code)
        {
            HttpBody body = new HttpBody();
            body.Add("code", code);
            body.Add("redirect_uri", _redirectUri);
            body.Add("client_secret", _clientSecret);
            body.Add("client_id", _clientId);
            body.Add("grant_type", "authorization_code");

            HttpClient client = _httpClientFactory.CreateClient();

            return await client.SendRequest(_amazonAuth, HttpMethod.Post, body);
        }
    }
}