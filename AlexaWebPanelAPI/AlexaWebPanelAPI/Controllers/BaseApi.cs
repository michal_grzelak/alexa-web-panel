﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AlexaWebPanelAPI.Controllers
{
    public class BaseApi : ControllerBase
    {
        protected readonly IHttpClientFactory _httpClientFactory;
        protected readonly IConfiguration _configuration;

        protected readonly string _amazonAuth;
        protected readonly string _amazonApi;
        protected readonly string _clientId;
        protected readonly string _clientSecret;
        protected readonly string _redirectUri;

        public BaseApi(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;

            _amazonAuth = configuration.GetSection("Amazon").GetValue<string>("AuthUrl");
            _amazonApi = configuration.GetSection("Amazon").GetValue<string>("ApiUrl");
            _clientId = configuration.GetSection("Amazon").GetValue<string>("ClientId");
            _clientSecret = configuration.GetSection("Amazon").GetValue<string>("ClientSecret");
            _redirectUri = configuration.GetSection("Amazon").GetValue<string>("RedirectUri");
        }
    }
}
