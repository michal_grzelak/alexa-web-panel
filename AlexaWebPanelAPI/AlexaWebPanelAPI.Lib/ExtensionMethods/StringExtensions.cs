﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlexaWebPanelAPI.Lib.ExtensionMethods
{
    public static class StringExtensions
    {
        public static object ToJsonObject(this string jsonString)
        {
            return JsonConvert.DeserializeObject(jsonString);
        }

        public static string ToJsonString(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
