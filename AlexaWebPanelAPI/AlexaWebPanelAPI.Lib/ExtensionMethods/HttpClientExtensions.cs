﻿using AlexaWebPanelAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AlexaWebPanelAPI.Lib.ExtensionMethods
{
    public static class HttpClientExtensions
    {
        public static async Task<IActionResult> SendRequest(this HttpClient httpClient, string url, HttpMethod httpMethod, HttpBody httpBody)
        {
            HttpRequestMessage request = new HttpRequestMessage(httpMethod, url)
            {
                Content = new FormUrlEncodedContent(httpBody.Json)
            };

            HttpResponseMessage response = await httpClient.SendAsync(request);

            return new JsonResult((await response.Content.ReadAsStringAsync()).ToJsonObject());
        }

        public static async Task<IActionResult> SendRequest(this HttpClient httpClient, string url, HttpMethod httpMethod, IEnumerable<KeyValuePair<string, string>> headers)
        {
            HttpRequestMessage request = new HttpRequestMessage(httpMethod, url);

            foreach(KeyValuePair<string, string> header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            HttpResponseMessage response = await httpClient.SendAsync(request);

            return new JsonResult((await response.Content.ReadAsStringAsync()).ToJsonObject());
        }
    }
}
