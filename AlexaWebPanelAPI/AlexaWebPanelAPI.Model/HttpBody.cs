﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace AlexaWebPanelAPI.Model
{
    public class HttpBody
    {
        public Dictionary<string, string> Json { get; private set; }

        public HttpBody()
        {
            Json = new Dictionary<string, string>();
        }

        public void Add(string key, string value)
        {
            Json.Add(key, value);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(Json);
        }
    }
}
