export class Vendor {
    id: string;
    name: string;
    roles: string[];
}
