import { TestBed, inject } from '@angular/core/testing';

import { AmazonManagementService } from './amazon-management.service';

describe('AmazonManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AmazonManagementService]
    });
  });

  it('should be created', inject([AmazonManagementService], (service: AmazonManagementService) => {
    expect(service).toBeTruthy();
  }));
});
