import { Component, OnInit } from '@angular/core';
import { Vendor } from '../models/vendor';
import { AmazonManagementService } from '../amazon-management.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(
    private _amazonManagementService: AmazonManagementService
  ) { }

  vendorList: Vendor[];

  ngOnInit() {
    this._amazonManagementService
      .getVendorList()
      .subscribe(result => {
        this.vendorList = result.vendors;
      });
  }

}
