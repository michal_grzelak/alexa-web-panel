import { Component, OnInit } from '@angular/core';
import { AmazonManagementService } from '../amazon-management.service';
import { environment } from '../../environments/environment.prod';

declare var amazon: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private _amazonManagementService: AmazonManagementService
  ) { }

  ngOnInit() {

  }

  loginToAmazon() {
    const _this = this;
    const options = { scope: 'alexa::ask:skills:readwrite', response_type: 'code', redirect_uri: `${environment.redirectUri}` };
    amazon.Login.authorize(options, function (response) {
      console.log(response);
      _this._amazonManagementService
        .getAccessToken(response.code)
        .subscribe(result => {
          console.log(result);
          if (result['access_token']) {
            localStorage.setItem('access_token', result.access_token);
            localStorage.setItem('refresh_token', result.refresh_token);
            localStorage.setItem('token_date', Date.now().toString());
          }
        });
    });

    return false;
  }

  refreshToken() {
    const refreshToken = localStorage.getItem('refresh_token');

    this._amazonManagementService
      .getRefreshedAccessToken(refreshToken)
      .subscribe(result => {
        console.log(result);

        if (result['access_token']) {
          localStorage.setItem('access_token', result.access_token);
          localStorage.setItem('token_date', Date.now().toString());
        }
      });
  }
}
