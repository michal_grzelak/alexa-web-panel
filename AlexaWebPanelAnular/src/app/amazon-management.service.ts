import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment.prod';
import { Vendor } from './models/vendor';

@Injectable({
  providedIn: 'root'
})
export class AmazonManagementService {

  constructor(
    private http: HttpClient
  ) { }

  getAccessToken(responseCode: string): Observable<any> {
    return this.http.get(`${environment.baseApi}/amazon/auth/token/${responseCode}`);
  }

  getRefreshedAccessToken(refreshToken: string): Observable<any> {
    const header = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.http.post(`${environment.baseApi}/amazon/auth/token/refresh`,
      JSON.stringify(refreshToken),
      { headers: header});
  }

  getVendorList(): Observable<any> {
    const header = new HttpHeaders()
      .set('Authorization', `${localStorage.getItem('access_token')}`);

    return this.http.get(`${environment.baseApi}/amazon/api/v1/vendors`);
  }
}
