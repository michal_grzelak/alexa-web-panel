export const environment = {
  production: true,
  clientId: 'amzn1.application-oa2-client.d4905f9deedb4193a369e4ea48b1a770',
  clientSecret: '0b9a17b59582e38b070213d82d53f8e7080623b26bbe9f2ba14c515ff7f912f7',
  redirectUri: '/login',
  tokenUrl: 'https://api.amazon.com/auth/o2/token',
  amazonApi: 'https://api.amazonalexa.com',
  baseApi: 'https://localhost:44372'
};
